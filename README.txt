=== Toy Instagram ===
Contributors: thewhodidthis
Tags: instagram
Requires at least: 3.0.1
Tested up to: 5.5
Stable tag: 0.0
License: Unlicense
License URI: http://unlicense.org/

Helps produce "instarolls" via shortcode.
