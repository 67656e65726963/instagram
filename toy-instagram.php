<?php

/**
 * Plugin Name:       Toy Instagram
 * Plugin URI:        https://gitlab.com/67656e65726963/toy-instagram
 * Description:       Helps present your latest instagram posts.
 * Version:           0.0.0
 * Author:            67656e65726963
 * Author URI:        https://gitlab.com/67656e65726963
 * License:           Unlicense
 * License URI:       http://unlicense.org/
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

add_shortcode(
	'instagram',
	function( $atts, $pass_maybe = '' ) {
		$pass = get_option( 'toy_instagram', $pass_maybe );

		if ( isset( $atts['pass'] ) ) {
			$pass = wp_strip_all_tags( $atts['pass'] );
		} elseif ( isset( $atts[0] ) ) {
			$pass = $atts[0];
		}

		if ( isset( $pass ) && strlen( $pass ) > 0 ) {
			$hook = sprintf( 'https://api.instagram.com/v1/users/self/media/recent/?access_token=%1$s', $pass );
			$data = wp_remote_get( $hook );
			$body = wp_remote_retrieve_body( $data );

			if ( empty( $body ) ) {
				return;
			}

			$json = json_decode( $body, true );
			$list = $json['data'];

			$roll = array_map(
				function( $item ) {
					$node = $item['images']['standard_resolution'];

					return sprintf(
						'<figure class="gallery-item">
							<div class="gallery-icon">
								<img src="%1$s" width="%2$s" height="%3$s">
							</div>
						</figure>',
						$node['url'],
						$node['width'],
						$node['height']
					);
				},
				$list
			);

			return sprintf( '<figure class="gallery">%s</figure>', implode( $roll ) );
		}
	},
	67
);

add_action(
	'customize_register',
	function( $wp_customize ) {
		$wp_customize->add_setting(
			'toy_instagram',
			array(
				'default'   => '',
				'type'      => 'option',
				'transport' => 'refresh',
			)
		);

		$wp_customize->add_control(
			'toy_instagram',
			array(
				'label'   => 'Instagram',
				'section' => 'title_tagline',
			)
		);
	},
	67
);

add_action(
	'admin_print_footer_scripts',
	function() {
		if ( wp_script_is( 'quicktags' ) ) : ?>
			<script type="text/javascript">
				QTags.addButton( 'eg_instagram', 'insta', '[insta]', '[/insta]', 'instagram', 'Instagram', 201 );
			</script>
			<?php
		endif;
	},
	67
);
